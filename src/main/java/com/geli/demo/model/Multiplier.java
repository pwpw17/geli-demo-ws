package com.geli.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity
public class Multiplier {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	Long id;

	@Column
	Float minYear;

	@Column
	Float maxYear;

	@Column
	int multiplier;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getMinYear() {
		return minYear;
	}

	public void setMinYear(Float minYear) {
		this.minYear = minYear;
	}

	public Float getMaxYear() {
		return maxYear;
	}

	public void setMaxYear(Float maxYear) {
		this.maxYear = maxYear;
	}

	public int getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(int multiplier) {
		this.multiplier = multiplier;
	}

}
