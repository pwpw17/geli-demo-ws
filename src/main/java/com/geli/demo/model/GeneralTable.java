package com.geli.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity
public class GeneralTable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	@Column
	String generalKey;

	@Column
	String generalValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGeneralKey() {
		return generalKey;
	}

	public void setGeneralKey(String generalKey) {
		this.generalKey = generalKey;
	}

	public String getGeneralValue() {
		return generalValue;
	}

	public void setGeneralValue(String generalValue) {
		this.generalValue = generalValue;
	}

}
