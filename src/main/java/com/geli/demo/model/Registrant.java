package com.geli.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity
public class Registrant {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	@Column
	String membership;

	@Column
	String gender;

	@Column
	Date birthdate;

	@Column
	String pricingPlan;

	@Column
	int multiplier;

	@Column
	String price;

	@Column
	String fullName;

	@Column
	String phone;

	@Column
	String email;

	@Column
	String birthplace;

	@Column
	String identityType;

	@Column
	String identityNumber;

	@Column
	boolean isAddressEqual;

	@Column
	String identityAddress;

	@Column
	String identityProvince;

	@Column
	String homeAddress;

	@Column
	String homeProvince;

	@Column
	String referenceCode;

	@Column
	boolean isWorking;

	@Column
	String addressForMailing;

	@Column
	int bodyHeight;

	@Column
	int bodyWeight;

	@Column
	boolean isAtRisk;

	@Column
	Double benefitTotal;

	@Column
	Double wakafRate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMembership() {
		return membership;
	}

	public void setMembership(String membership) {
		this.membership = membership;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getPricingPlan() {
		return pricingPlan;
	}

	public void setPricingPlan(String pricingPlan) {
		this.pricingPlan = pricingPlan;
	}

	public int getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(int multiplier) {
		this.multiplier = multiplier;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthplace() {
		return birthplace;
	}

	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	public String getIdentityNumber() {
		return identityNumber;
	}

	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}

	public boolean isAddressEqual() {
		return isAddressEqual;
	}

	public void setAddressEqual(boolean isAddressEqual) {
		this.isAddressEqual = isAddressEqual;
	}

	public String getIdentityAddress() {
		return identityAddress;
	}

	public void setIdentityAddress(String identityAddress) {
		this.identityAddress = identityAddress;
	}

	public String getIdentityProvince() {
		return identityProvince;
	}

	public void setIdentityProvince(String identityProvince) {
		this.identityProvince = identityProvince;
	}

	public String getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}

	public String getHomeProvince() {
		return homeProvince;
	}

	public void setHomeProvince(String homeProvince) {
		this.homeProvince = homeProvince;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public boolean isWorking() {
		return isWorking;
	}

	public void setWorking(boolean isWorking) {
		this.isWorking = isWorking;
	}

	public String getAddressForMailing() {
		return addressForMailing;
	}

	public void setAddressForMailing(String addressForMailing) {
		this.addressForMailing = addressForMailing;
	}

	public int getBodyHeight() {
		return bodyHeight;
	}

	public void setBodyHeight(int bodyHeight) {
		this.bodyHeight = bodyHeight;
	}

	public int getBodyWeight() {
		return bodyWeight;
	}

	public void setBodyWeight(int bodyWeight) {
		this.bodyWeight = bodyWeight;
	}

	public boolean isAtRisk() {
		return isAtRisk;
	}

	public void setAtRisk(boolean isAtRisk) {
		this.isAtRisk = isAtRisk;
	}

	public Double getBenefitTotal() {
		return benefitTotal;
	}

	public void setBenefitTotal(Double benefitTotal) {
		this.benefitTotal = benefitTotal;
	}

	public Double getWakafRate() {
		return wakafRate;
	}

	public void setWakafRate(Double wakafRate) {
		this.wakafRate = wakafRate;
	}

}
