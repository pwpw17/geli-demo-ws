package com.geli.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.geli.demo.model.GeneralTable;

public interface GeneralTableRepo extends JpaRepository<GeneralTable, Long>{
	
	GeneralTable findTop1ByGeneralKey(String key);

}
