package com.geli.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.geli.demo.model.Product;

public interface ProductRepo extends JpaRepository<Product, Long> {

}
