package com.geli.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.geli.demo.model.Multiplier;

public interface MultiplierRepo extends JpaRepository<Multiplier, Long>{
	
	Multiplier findByMinYearLessThanEqualAndMaxYearGreaterThanEqual(float year1, float year2);

}
