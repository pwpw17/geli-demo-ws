package com.geli.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.geli.demo.model.Registrant;

public interface RegistrantRepo extends JpaRepository<Registrant, Long> {

}
