package com.geli.demo.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.geli.demo.model.GeneralTable;
import com.geli.demo.model.Multiplier;
import com.geli.demo.model.Product;
import com.geli.demo.repository.GeneralTableRepo;
import com.geli.demo.repository.MultiplierRepo;
import com.geli.demo.repository.ProductRepo;

@Component
public class DataLoader implements ApplicationRunner{

	@Autowired
	MultiplierRepo multiplierRepo;
	
	@Autowired
	ProductRepo productRepo;
	
	@Autowired
	GeneralTableRepo gtRepo;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
		Long countMultiplier = multiplierRepo.count();
		Long countProduct = productRepo.count();
		Long countGeneralTable = gtRepo.count();
		
		if(countProduct == 0 ) {
			Product product = new Product();
			product.setId((long) 1);
			product.setProductName("IGHAS1");
			productRepo.save(product);
			
			product = new Product();
			product.setId((long) 2);
			product.setProductName("IGHAS2");
			productRepo.save(product);

			product = new Product();
			product.setId((long) 3);
			product.setProductName("IGHAS3");
			productRepo.save(product);
			
			product = new Product();
			product.setId((long) 4);
			product.setProductName("IGHAS4");
			productRepo.save(product);
			
			product = new Product();
			product.setId((long) 5);
			product.setProductName("TBC");
			productRepo.save(product);
		}
		
		
		if(countMultiplier==0) {
			Multiplier multiplier = new Multiplier();
			multiplier.setMinYear((float) 0.14);
			multiplier.setMaxYear((float) 24);
			multiplier.setMultiplier(48);
			multiplierRepo.save(multiplier);
			
			multiplier = new Multiplier();
			multiplier.setMinYear((float) 25);
			multiplier.setMaxYear((float) 34);
			multiplier.setMultiplier(24);
			multiplierRepo.save(multiplier);
			
			multiplier = new Multiplier();
			multiplier.setMinYear((float) 35);
			multiplier.setMaxYear((float) 44);
			multiplier.setMultiplier(12);
			multiplierRepo.save(multiplier);
			
			multiplier = new Multiplier();
			multiplier.setMinYear((float) 45);
			multiplier.setMaxYear((float) 50);
			multiplier.setMultiplier(6);
			multiplierRepo.save(multiplier);

			multiplier = new Multiplier();
			multiplier.setMinYear((float) 51);
			multiplier.setMaxYear((float) 99999);
			multiplier.setMultiplier(5);
			multiplierRepo.save(multiplier);
		}
		
		if(countGeneralTable==0) {
			GeneralTable generalTable = new GeneralTable();
			generalTable.setGeneralKey("home_label");
			generalTable.setGeneralValue("COVID-19 CUSTOMER CARE FUND, Inisiatif Perlindungan Komprehensif, Terhadap kasus infeksi COVID-19 untuk Anda");
			gtRepo.save(generalTable);
		}
	}

}
