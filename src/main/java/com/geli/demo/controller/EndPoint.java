package com.geli.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMessage;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.geli.demo.common.Age;
import com.geli.demo.common.AgeCalculation;
import com.geli.demo.model.GeneralTable;
import com.geli.demo.model.Multiplier;
import com.geli.demo.model.Registrant;
import com.geli.demo.repository.GeneralTableRepo;
import com.geli.demo.repository.MultiplierRepo;
import com.geli.demo.repository.RegistrantRepo;

@RestController
@CrossOrigin
public class EndPoint {

	@Autowired
	MultiplierRepo multiplierRepo;

	@Autowired
	RegistrantRepo regRepo;
	
	@Autowired
	GeneralTableRepo gtRepo;

	@GetMapping("/multiplier/{data}")
	public HashMap<String, Object> multiplier(@PathVariable String data) throws ParseException {
		HashMap<String, Object> ret = new HashMap<>();
		float year = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date birthDate = sdf.parse(data);

		Age age = AgeCalculation.calculateAge(birthDate);
		if (age.getYears() == 0) {
			year = (float) age.getDays() / 100;
		} else {
			year = age.getYears();
		}
		Multiplier result = multiplierRepo.findByMinYearLessThanEqualAndMaxYearGreaterThanEqual(year, year);

		if (result != null) {
			ret.put("multiplier", result.getMultiplier());
		} else {
			ret.put("multiplier", 0);
		}
		return ret;
	}

	@PostMapping("/registrants")
	public HashMap<String, Object> registrants(@RequestBody Registrant request) {
		HashMap<String, Object> ret = new HashMap<>();

		Registrant result = regRepo.save(request);

		if (result.getId() != null) {
			ret.put("ID", result.getId());
			ret.put("message", HttpStatus.OK);
		} else {
			ret.put("message", HttpStatus.BAD_REQUEST);
		}

		return ret;
	}

	@GetMapping("/registrants/{id}")
	public Registrant findById(@PathVariable Long id) {
		Registrant result = regRepo.findById(id).get();
		return result;
	}

	@GetMapping("/registrants")
	public List<Registrant> findAll() {
		List<Registrant> result = regRepo.findAll();
		return result;
	}

	@PutMapping("/registrants/{id}")
	public HashMap<String, Object> update(@PathVariable Long id, @RequestBody Registrant request) {
		HashMap<String, Object> ret = new HashMap<>();

		try {
			Registrant result = regRepo.findById(id).get();
			request.setId(result.getId());
			regRepo.save(request);
			ret.put("message", HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			ret.put("message", HttpStatus.BAD_REQUEST);
		}

		return ret;
	}

	@DeleteMapping("/registrants/{id}")
	public HashMap<String, Object> update(@PathVariable Long id) {
		HashMap<String, Object> ret = new HashMap<>();

		try {
			Registrant result = regRepo.findById(id).get();
			regRepo.deleteById(id);
			ret.put("message", HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			ret.put("message", HttpStatus.BAD_REQUEST);
		}

		return ret;
	}
	
	@GetMapping("/getLabel")
	public Object getLabel() {
		HashMap<String, String> ret = new HashMap<String, String>();
		String result = gtRepo.findTop1ByGeneralKey("home_label").getGeneralValue();
		ret.put("message", result);
		return ret;
	}
	
	@PostMapping("/updateHomeLabel")
	public Object updateHomeLabel(@RequestBody GeneralTable request) {
		HashMap<String, Object> ret = new HashMap<String, Object>();
		GeneralTable result = gtRepo.findTop1ByGeneralKey("home_label");
		result.setGeneralValue(request.getGeneralValue());
		gtRepo.save(result);
		ret.put("message", HttpStatus.OK);
		return ret;
	}

}
